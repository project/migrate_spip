<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test for SPIP rich text plugins.
 *
 * @group migrate_spip
 */
final class SpipRichTextTest extends KernelTestBase {

  /**
   * The SPIP rich text filepath to test.
   *
   * @var string
   */
  const FILEPATH_TEXT = __DIR__ . '/../../data/spip-rich-text.txt';

  /**
   * The SPIP rich text expected html filepath.
   *
   * @var string
   */
  const FILEPATH_HTML = __DIR__ . '/../../data/expected-html.txt';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate_spip',
  ];

  /**
   * Test SPIP rich text conversion with all enabled plugins.
   */
  public function testApplyAll(): void {
    // Set config to prevent error.
    $this->config('migrate_spip.settings')
      ->set('plugins_disabled', [])
      ->save();

    $text = $html = '';

    $handleText = fopen(self::FILEPATH_TEXT, 'r');
    if ($handleText) {
      $text = fread(
        $handleText,
        filesize(self::FILEPATH_TEXT)
      );
    }

    $handleHtml = fopen(self::FILEPATH_HTML, 'r');
    if ($handleHtml) {
      $html = fread(
        $handleHtml,
        filesize(self::FILEPATH_HTML)
      );
    }

    $this->assertSame(
      trim($html),
      $this->container->get('plugin.manager.spip_rich_text')
        ->applyAll($text)
    );
  }

}
