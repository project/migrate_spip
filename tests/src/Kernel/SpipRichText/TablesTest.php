<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "tables" plugin.
 *
 * @group migrate_spip
 */
final class TablesTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '||Caption| multiline||
|{{Column1}}| {{Column2}} | {{Column3}} |
|L1C1| L1C2 | L1C3 |
| L2C1 | L2C2 et L2C3 |<|
| L3C1 |^| L3C3 |',
        "\n\n\n" . '<table aria-describedby="test">
<caption>Caption<br /> <small id="test" class="summary offscreen">multiline</small></caption>
<thead><tr class="row_first"><th id="idtest_c0">Column1</th><th id="idtest_c1">Column2</th><th id="idtest_c2">Column3</th></tr></thead>
<tbody>
<tr class="row_even even">
<td headers="idtest_c0">L1C1</td>
<td headers="idtest_c1">L1C2</td>
<td headers="idtest_c2">L1C3</td></tr>
<tr class="row_odd odd">
<td headers="idtest_c0">L2C1</td>
<td colspan="2" rowspan="2" headers="idtest_c1">L2C2 et L2C3</td></tr>
<tr class="row_even even">
<td headers="idtest_c0">L3C1</td>
<td headers="idtest_c2">L3C3</td></tr>
</tbody>
</table>' . "\n\n\n",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'tables';
  }

  /**
   * {@inheritdoc}
   *
   * @dataProvider applyProvider
   */
  public function testApply(string $text, string $html): void {
    // Set config to prevent error.
    $this->config('migrate_spip.settings')
      ->set('plugins_disabled', [])
      ->save();

    $applied = $this->container->get('plugin.manager.spip_rich_text')
      ->apply($this->getPluginId(), $text);
    $this->assertSame(
      $html,
      // Fix all random identifiers.
      preg_replace(
        [
          '#aria-describedby="([a-z0-9]+)"#',
          '#<small id="([a-z0-9]+)"#',
          '#="id([a-z0-9]+)_#s',
        ],
        [
          'aria-describedby="test"',
          '<small id="test"',
          '="idtest_',
        ],
        $applied
      )
    );
  }

}
