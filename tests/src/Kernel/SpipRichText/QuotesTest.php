<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "quotes" plugin.
 *
 * @group migrate_spip
 */
final class QuotesTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '<quote>Lorem ipsum</quote>',
        '<blockquote>Lorem ipsum</blockquote>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'quotes';
  }

}
