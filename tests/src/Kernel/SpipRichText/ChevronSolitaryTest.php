<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "chevron_solitary" plugin.
 *
 * @group migrate_spip
 */
final class ChevronSolitaryTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '<lorem',
        '&lt;lorem',
      ],
      [
        '</lorem>',
        '</lorem>',
      ],
      [
        '< Lorem ipsum',
        '&lt; Lorem ipsum',
      ],
      [
        '< Lorem ipsum < Lorem ipsum',
        '&lt; Lorem ipsum &lt; Lorem ipsum',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'chevron_solitary';
  }

}
