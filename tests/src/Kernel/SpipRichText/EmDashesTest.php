<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "em_dashes" plugin.
 *
 * @group migrate_spip
 */
final class EmDashesTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '--Lorem ipsum',
        '--Lorem ipsum',
      ],
      [
        '-- Lorem ipsum',
        '-- Lorem ipsum',
      ],
      [
        "\n--Lorem ipsum",
        "\n<br />&mdash;&nbsp;Lorem ipsum",
      ],
      [
        "\n-- Lorem ipsum",
        "\n<br />&mdash;&nbsp;Lorem ipsum",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'em_dashes';
  }

}
