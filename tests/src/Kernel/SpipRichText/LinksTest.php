<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "links" plugin.
 *
 * @group migrate_spip
 */
final class LinksTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '[Lorem ipsum->https://www.drupal.org/]',
        '<a href="https://www.drupal.org/">Lorem ipsum</a>',
      ],
      [
        '[Lorem ipsum{with language}->https://www.drupal.org/]',
        '<a href="https://www.drupal.org/" lang="with language">Lorem ipsum</a>',
      ],
      [
        '[Lorem ipsum|with title->https://www.drupal.org/]',
        '<a href="https://www.drupal.org/" title="with title">Lorem ipsum</a>',
      ],
      [
        '[Lorem ipsum|with title{with language}->https://www.drupal.org/]',
        '<a href="https://www.drupal.org/" title="with title" lang="with language">Lorem ipsum</a>',
      ],
      [
        '[->https://www.drupal.org/]',
        '<a href="https://www.drupal.org/">https://www.drupal.org/</a>',
      ],
      [
        '[lorem-ipsum<-]',
        '<a id="lorem-ipsum"></a>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'links';
  }

}
