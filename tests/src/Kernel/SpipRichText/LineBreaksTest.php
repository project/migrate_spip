<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "line_breaks" plugin.
 *
 * @group migrate_spip
 */
final class LineBreaksTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        "\r",
        "\n",
      ],
      [
        "\r\n",
        "\n",
      ],
      [
        "_ Lorem ipsum",
        'Lorem ipsum',
      ],
      [
        "\n_ Lorem ipsum",
        "\nLorem ipsum",
      ],
      [
        "\n\n_ Lorem ipsum",
        "\n\nLorem ipsum",
      ],
      [
        "Lorem\n_ ipsum",
        'Lorem<br class="manualbr" />ipsum',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'line_breaks';
  }

}
