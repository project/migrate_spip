<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "align_right" plugin.
 *
 * @group migrate_spip
 */
final class AnchorsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '[#lorem-ipsum-anchor<-]',
        '<span id="lorem-ipsum-anchor"></span>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'anchors';
  }

}
