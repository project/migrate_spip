<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "poesie" plugin.
 *
 * @group migrate_spip
 */
final class PoesieTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '<poesie>Lorem ipsum</poesie>',
        "<blockquote class=\"poesie\"><p>Lorem ipsum</p></blockquote>\n\n",
      ],
      [
        '<poetry>Lorem ipsum</poetry>',
        "<blockquote class=\"poesie\"><p>Lorem ipsum</p></blockquote>\n\n",
      ],
      [
        "<poesie>Lorem\nipsum</poesie>",
        "<blockquote class=\"poesie\"><p>Lorem</p>\n<p>ipsum</p></blockquote>\n\n",
      ],
      [
        "<poesie>Lorem\r\nipsum</poesie>",
        "<blockquote class=\"poesie\"><p>Lorem</p>\n<p>ipsum</p></blockquote>\n\n",
      ],
      [
        "<poesie>Lorem\n\n\nipsum</poesie>",
        "<blockquote class=\"poesie\"><p>Lorem</p>\n<p>&nbsp;</p>\n<p>ipsum</p></blockquote>\n\n",
      ],
      [
        "<poesie>Lorem\n \nipsum</poesie>",
        "<blockquote class=\"poesie\"><p>Lorem</p>\n<p>&nbsp;</p>\n<p>ipsum</p></blockquote>\n\n",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'poesie';
  }

}
