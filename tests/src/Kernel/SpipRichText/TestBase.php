<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test base for SPIP rich text plugins.
 *
 * @group migrate_spip
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class TestBase extends KernelTestBase implements TestInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate_spip',
  ];

  /**
   * Get SPIP rich text plugin identifier.
   *
   * @return string
   *   The plugin identifier.
   */
  abstract protected function getPluginId(): string;

  /**
   * {@inheritdoc}
   *
   * @dataProvider applyProvider
   *
   * @throws \Exception
   */
  public function testApply(string $text, string $html): void {
    // Set config to prevent error.
    $this->config('migrate_spip.settings')
      ->set('plugins_disabled', [])
      ->save();

    $this->assertSame(
      $html,
      $this->container->get('plugin.manager.spip_rich_text')
        ->apply($this->getPluginId(), $text)
    );
  }

}
