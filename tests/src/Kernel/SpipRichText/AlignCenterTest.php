<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "align_center" plugin.
 *
 * @group migrate_spip
 */
final class AlignCenterTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '[|Lorem ipsum|]',
        '<p class="text-align-center">Lorem ipsum</p>',
      ],
      [
        '<center>Lorem ipsum</center>',
        '<p class="text-align-center">Lorem ipsum</p>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'align_center';
  }

}
