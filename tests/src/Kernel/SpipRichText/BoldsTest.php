<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "bolds" plugin.
 *
 * @group migrate_spip
 */
final class BoldsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        // Empty bolds.
        '{{}}',
        '',
      ],
      [
        '{{Lorem ipsum}}',
        '<strong>Lorem ipsum</strong>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'bolds';
  }

}
