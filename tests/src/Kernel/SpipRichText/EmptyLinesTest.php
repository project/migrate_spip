<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "empty_lines" plugin.
 *
 * @group migrate_spip
 */
final class EmptyLinesTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        "Lorem\n\nipsum",
        "Lorem\n\nipsum",
      ],
      [
        "Lorem\n ipsum",
        "Lorem\n ipsum",
      ],
      [
        "Lorem\n\tipsum",
        "Lorem\n\tipsum",
      ],
      [
        "Lorem\n \nipsum",
        "Lorem\n\nipsum",
      ],
      [
        "Lorem\n\t\nipsum",
        "Lorem\n\nipsum",
      ],
      [
        "Lorem\n\t \nipsum",
        "Lorem\n\nipsum",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'empty_lines';
  }

}
