<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "preformated_code" plugin.
 *
 * @group migrate_spip
 */
final class PreFormatedCodeTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '`Computing <strong>code</strong>`',
        '<code>Computing &lt;strong&gt;code&lt;/strong&gt;</code>',
      ],
      [
        "```\nPreformatted <strong>code</strong>\n```",
        '<pre><code>Preformatted &lt;strong&gt;code&lt;/strong&gt;</code></pre>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'preformated_code';
  }

}
