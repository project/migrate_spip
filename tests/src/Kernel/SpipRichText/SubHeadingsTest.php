<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "subheadings" plugin.
 *
 * @group migrate_spip
 */
final class SubHeadingsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    $data = [];
    for ($index = 2; $index <= 6; $index++) {
      $asterisk = ($index > 2 ? str_repeat('*', $index - 1) : '');
      $data[] = [
        '{{{' . $asterisk . 'Lorem ipsum}}}',
        '<h' . $index . '>Lorem ipsum</h' . $index . '>',
      ];
      // Wrapped with spaces.
      $data[] = [
        '{{{' . $asterisk . ' Lorem ipsum }}}',
        '<h' . $index . '>Lorem ipsum</h' . $index . '>',
      ];
      // Prefixed by "|".
      $data[] = [
        '|{{{' . $asterisk . 'Lorem ipsum}}}',
        '<h' . $index . '>Lorem ipsum</h' . $index . '>',
      ];
      // Prefixed by "|" and wrapped with spaces.
      $data[] = [
        '|{{{' . $asterisk . ' Lorem ipsum }}}',
        '<h' . $index . '>Lorem ipsum</h' . $index . '>',
      ];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'subheadings';
  }

}
