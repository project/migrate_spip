<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "lists" plugin.
 *
 * @group migrate_spip
 */
final class ListsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        "\n-*Lorem ipsum",
        '<ul><li>Lorem ipsum</li></ul>',
      ],
      [
        "\n-*Lorem ipsum 1\n-**Lorem ipsum 1.1",
        '<ul><li>Lorem ipsum 1<ul><li>Lorem ipsum 1.1</li></ul></li></ul>',
      ],
      [
        "\n-*Lorem ipsum 1\n-##Lorem ipsum 1.1",
        '<ul><li>Lorem ipsum 1<ol><li>Lorem ipsum 1.1</li></ol></li></ul>',
      ],
      [
        "\n-#Lorem ipsum",
        '<ol><li>Lorem ipsum</li></ol>',
      ],
      [
        "\n-#Lorem ipsum 1\n-##Lorem ipsum 1.1",
        '<ol><li>Lorem ipsum 1<ol><li>Lorem ipsum 1.1</li></ol></li></ol>',
      ],
      [
        "\n-#Lorem ipsum 1\n-**Lorem ipsum 1.1",
        '<ol><li>Lorem ipsum 1<ul><li>Lorem ipsum 1.1</li></ul></li></ol>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'lists';
  }

}
