<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "align_right" plugin.
 *
 * @group migrate_spip
 */
final class AlignRightTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '[/Lorem ipsum/]',
        '<p class="text-align-right">Lorem ipsum</p>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'align_right';
  }

}
