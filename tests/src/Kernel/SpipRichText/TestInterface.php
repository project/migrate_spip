<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Interface for test SPIP rich text plugins.
 *
 * @group migrate_spip
 */
interface TestInterface {

  /**
   * Provides test data for ::testApply().
   *
   * @return array
   *   An array of test cases, each test case is a texte to test. with two values:
   *   0. A string containing the SPIP rich text to test.
   *   1. A string containing the expected HTML result.
   */
  public static function applyProvider(): array;

  /**
   * Test SPIP rich text conversion.
   *
   * @param string $text
   *   A list of pages that should be tracked. One page per line.
   * @param string $html
   *   TRUE if a validation error should be thrown. FALSE otherwise.
   */
  public function testApply(string $text, string $html): void;

}
