<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "horizontal_rules" plugin.
 *
 * @group migrate_spip
 */
final class HorizontalRulesTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        "Lorem ipsum\n---",
        "Lorem ipsum\n---",
      ],
      [
        "---\nLorem ipsum",
        "---\nLorem ipsum",
      ],
      [
        "Lorem ipsum\n___",
        "Lorem ipsum\n___",
      ],
      [
        "___\nLorem ipsum",
        "___\nLorem ipsum",
      ],
      [
        "Lorem ipsum\n----",
        "Lorem ipsum\n\n\n<hr />\n\n\n",
      ],
      [
        "----\nLorem ipsum",
        "\n\n\n<hr />\n\n\n\nLorem ipsum",
      ],
      [
        "Lorem ipsum\n_____",
        "Lorem ipsum\n\n\n<hr />\n\n\n",
      ],
      [
        "_____\nLorem ipsum",
        "\n\n\n<hr />\n\n\n\nLorem ipsum",
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'horizontal_rules';
  }

}
