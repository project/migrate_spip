<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "italics" plugin.
 *
 * @group migrate_spip
 */
final class ItalicsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        // Empty bolds.
        '{}',
        '',
      ],
      [
        '{Lorem ipsum}',
        '<em>Lorem ipsum</em>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'italics';
  }

}
