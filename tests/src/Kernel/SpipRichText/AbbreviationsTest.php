<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "abbreviations" plugin.
 *
 * @group migrate_spip
 */
final class AbbreviationsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        '[abbr|Lorem ipsum]',
        '<abbr>Lorem ipsum</abbr>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'abbreviations';
  }

}
