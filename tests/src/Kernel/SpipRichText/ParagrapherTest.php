<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

/**
 * Test SPIP rich text "paragrapher" plugin.
 *
 * @group migrate_spip
 */
final class ParagrapherTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        // Without paragraph.
        'Lorem ipsum',
        '<p>Lorem ipsum</p>',
      ],
      [
        // Not closed paragraph.
        '<p>Lorem ipsum',
        '<p>Lorem ipsum</p>',
      ],
      [
        // Closed paragraph but no open.
        'Lorem ipsum</p>',
        '<p>Lorem ipsum</p>',
      ],
      [
        // Inline html tag wrapper without paragraph.
        '<strong>Lorem ipsum</strong>',
        '<p><strong>Lorem ipsum</strong></p>',
      ],
      [
        // Inline html tag wrapper with not closed paragraph.
        '<p><strong>Lorem ipsum</strong>',
        '<p><strong>Lorem ipsum</strong></p>',
      ],
      [
        // Inline html tag wrapper with closed paragraph but not opened.
        '<strong>Lorem ipsum</strong></p>',
        '<p><strong>Lorem ipsum</strong></p>',
      ],
      [
        // Don't wrap block html tag with paragraph.
        '<article>Lorem ipsum</article>',
        '<article>Lorem ipsum</article>',
      ],
      [
        // Don't wrap block html tag with paragraph.
        '<p><article>Lorem ipsum</article></p>',
        '<article>Lorem ipsum</article>',
      ],
      [
        // Block html tag wrapped by a link with paragraph.
        '<p><a href=""><article>Lorem ipsum</article></a></p>',
        '<a href=""><article>Lorem ipsum</article></a>',
      ],
      [
        // Block html tag wrapped by a link with not closed paragraph.
        '<p><a href=""><article>Lorem ipsum</article></a>',
        '<a href=""><article>Lorem ipsum</article></a>',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'paragrapher';
  }

}
