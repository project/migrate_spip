<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_spip\Kernel\SpipRichText;

use Drupal\migrate_spip\SpipRichTextInterface;

/**
 * Test SPIP rich text "paragraphs" plugin.
 *
 * @group migrate_spip
 */
final class ParagraphsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function applyProvider(): array {
    return [
      [
        "\n\n<br>",
        '<p>',
      ],
      [
        "\n \n <br/>",
        '<p> <br/>',
      ],
      [
        "\n\n\n<br />",
        '<p>',
      ],
      [
        '<br><br>',
        '<p>',
      ],
      [
      '<br/><br/>',
        '<p>',
      ],
      [
      '<br /><br />',
        '<p>',
      ],
      [
        SpipRichTextInterface::AUTOBR . SpipRichTextInterface::AUTOBR,
        '<p>',
      ],
      [
        '<p><br>',
        '<p>',
      ],
      [
        '<p><br/>',
        '<p>',
      ],
      [
        '<p><br />',
        '<p>',
      ],
      [
        '<p> <br />',
        '<p> <br />',
      ],
      [
        '<p> ' . SpipRichTextInterface::AUTOBR,
        '<p> ' . SpipRichTextInterface::AUTOBR,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginId(): string {
    return 'paragraphs';
  }

}
