<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;

/**
 * Source plugin for Media Images.
 *
 * @MigrateSource(
 *   id = "migrate_spip_media_images"
 * )
 */
class MediaImages extends MediaBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return parent::query()
      ->condition('extension', ['png', 'gif', 'jpg', 'jpeg', 'svg'], 'IN');
  }

}
