<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;

/**
 * Source plugin for Files.
 *
 * @MigrateSource(
 *   id = "migrate_spip_files"
 * )
 */
class Files extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'documents', 'd')
      ->fields('d', ['fichier', 'id_document']);
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'fichier' => $this->t('File'),
      'id_document' => $this->t('File ID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_document' => [
        'type' => 'integer',
        'alias' => 'd',
      ],
    ];
  }

}
