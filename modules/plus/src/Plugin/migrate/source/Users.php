<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for Users.
 *
 * @MigrateSource(
 *   id = "migrate_spip_users"
 * )
 */
class Users extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'auteurs', 'a');
    return $query->fields('a', [
      'email',
      'en_ligne',
      'id_auteur',
      'login',
      'maj',
      'statut',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'email' => $this->t('User mail'),
      'en_ligne' => $this->t('Access'),
      'id_auteur' => $this->t('Author ID'),
      'login'  => $this->t('User login'),
      'maj' => $this->t('Updated'),
      'statut' => $this->t('Status'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_auteur' => [
        'type' => 'integer',
        'alias' => 'a',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $row->setSourceProperty(
      'access',
      strtotime($row->getSourceProperty('en_ligne'))
    );
    $row->setSourceProperty(
      'changed',
      strtotime($row->getSourceProperty('maj'))
    );

    // By default, all authors are editors.
    // Assign "editor" role to each author.
    $row->setSourceProperty('roles', ['editor']);

    return parent::prepareRow($row);
  }

}
