<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for Tags.
 *
 * @MigrateSource(
 *   id = "migrate_spip_tags"
 * )
 */
class Tags extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    $query = $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'mots', 'm');
    $query->join($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'groupes_mots', 'sgm', 'm.id_groupe = sgm.id_groupe');
    return $query->fields('m', [
      'id_mot',
      'maj',
      'titre',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'id_mot' => $this->t('Word ID'),
      'maj' => $this->t('Updated'),
      'titre'  => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_mot' => [
        'type' => 'integer',
        'alias' => 'm',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $row->setSourceProperty(
      'changed',
      strtotime($row->getSourceProperty('maj'))
    );

    return parent::prepareRow($row);
  }

}
