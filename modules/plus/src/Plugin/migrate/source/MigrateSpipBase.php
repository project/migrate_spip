<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_spip\Helper\MigrateSpipHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an abstract class for migrate process.
 */
abstract class MigrateSpipBase extends SqlBase {

  /**
   * The SPIP migration helper.
   *
   * @var \Drupal\migrate_spip\Helper\MigrateSpipHelperInterface
   */
  protected MigrateSpipHelperInterface $migrateSpipHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ?MigrationInterface $migration = NULL
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration
    );
    $instance->migrateSpipHelper = $container->get('migrate_spip.helper');

    return $instance;
  }

  /**
   * Get object URL.
   *
   * @param string $objectType
   *   The object's type.
   * @param string $objectId
   *   The object's identifier.
   *
   * @return string
   *   The URL.
   */
  protected function getUrl(string $objectType, string $objectId): string {
    $urlInfo = $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'urls', 'su')
      ->fields('su', ['id_parent', 'url'])
      ->condition('su.id_objet', $objectId)
      ->condition('su.type', $objectType)
      ->orderBy('su.date', 'DESC')
      ->execute()
      ->fetchObject();

    $url = '';
    if ($urlInfo) {
      // SPIP url doesn't contain slash prefix. Add it and escape it in case.
      $url = '/' . ltrim($urlInfo->url, '/');

      if ($urlInfo->id_parent !== '0') {
        // Parent are sections only.
        $url = $this->getUrl('rubrique', $urlInfo->id_parent) . $url;
      }
    }

    return $url;
  }

}
