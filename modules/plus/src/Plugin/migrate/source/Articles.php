<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for Articles.
 *
 * @MigrateSource(
 *   id = "migrate_spip_articles"
 * )
 */
class Articles extends MigrateSpipBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ?MigrationInterface $migration = NULL
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $migration);
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'articles', 'a')
      ->fields('a', [
        'date',
        'id_article',
        'titre',
        'texte',
        'maj',
        'statut',
        'id_rubrique',
      ])
      // Do not import mirrors.
      ->condition('a.virtuel', '');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'date' => $this->t('Date'),
      'id_article' => $this->t('Article ID'),
      'id_rubrique' => $this->t('Section ID'),
      'maj' => $this->t('Updated'),
      'statut' => $this->t('Status'),
      'texte' => $this->t('Body text'),
      'titre' => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_article' => [
        'type' => 'integer',
        'alias' => 'a',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $row->setSourceProperty(
      'created',
      strtotime($row->getSourceProperty('date'))
    );
    $row->setSourceProperty(
      'changed',
      strtotime($row->getSourceProperty('maj'))
    );

    $row->setSourceProperty(
      'alias',
      $this->getUrl('article', $row->getSourceProperty('id_article'))
    );

    $this->prepareRowAuthor($row);
    $this->prepareRowTags($row);

    return parent::prepareRow($row);
  }

  /**
   * Prepare "author" property for given row.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   */
  private function prepareRowAuthor(Row $row): void {
    if (!$this->connection->schema()->tableExists('migrate_map_users')) {
      return;
    }

    // Search SPIP authors associated to current article.
    $sourceIds = $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'auteurs_liens', 'al')
      ->fields('al', ['id_auteur'])
      ->condition('al.objet', 'article')
      ->condition('al.id_objet', $row->getSourceProperty('id_article'))
      ->execute()
      ->fetchCol();

    if (empty($sourceIds)) {
      return;
    }

    // Get Drupal authors' identifier.
    $destId = $this->connection->select('migrate_map_users', 'mmu')
      ->fields('mmu', ['destid1'])
      ->condition('mmu.sourceid1', $sourceIds, 'IN')
      ->execute()
      // Drupal manage only one author. Keep first result.
      ->fetchField();

    if ($destId) {
      $row->setSourceProperty('author', $destId);
    }
  }

  /**
   * Prepare "tags" property for given row.
   *
   * @param \Drupal\migrate\Row $row
   *   The row object.
   */
  private function prepareRowTags(Row $row): void {
    if (!$this->connection->schema()->tableExists('migrate_map_tags')) {
      return;
    }

    // Search SPIP tags associated to current article.
    $sourceIds = $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'mots_liens', 'ml')
      ->fields('ml', ['id_mot'])
      ->condition('ml.objet', 'article')
      ->condition('ml.id_objet', $row->getSourceProperty('id_article'))
      ->execute()
      ->fetchCol();

    if (empty($sourceIds)) {
      return;
    }

    // Get Drupal tags' identifier.
    $destIds = $this->connection->select('migrate_map_tags', 'mmt')
      ->fields('mmt', ['destid1'])
      ->condition('mmt.sourceid1', $sourceIds, 'IN')
      ->execute()
      ->fetchCol();

    $tags = [];
    foreach ($destIds as $destId) {
      if (!empty($destId)) {
        $tags[] = ['target_id' => $destId];
      }
    }

    $row->setSourceProperty('tags', $tags);
  }

}
