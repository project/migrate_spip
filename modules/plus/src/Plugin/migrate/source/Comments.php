<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for Comments.
 *
 * @MigrateSource(
 *   id = "migrate_spip_comments"
 * )
 */
class Comments extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'forum', 'f')
      ->fields('f', [
        'auteur',
        'date_heure',
        'email_auteur',
        'id_forum',
        'id_objet',
        'id_parent',
        'statut',
        'maj',
        'name',
        'texte',
      ])
      // Only comments associated with articles.
      ->condition('f.objet', 'article')
      // Only offered or published comments.
      ->condition('f.statut', ['prop', 'publie'], 'IN')
      // Import forum without parent first to prevent skipping.
      ->orderBy('f.id_parent');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'auteur' => $this->t('Author (name)'),
      'date_heure' => $this->t('Date'),
      'email_auteur' => $this->t('Author (email)'),
      'id_objet' => $this->t('Article ID'),
      'id_forum' => $this->t('Forum ID'),
      'id_parent' => $this->t('Parent ID'),
      'maj' => $this->t('Updated'),
      'statut' => $this->t('Status'),
      'texte' => $this->t('Body text'),
      'titre'  => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_forum' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $row->setSourceProperty(
      'created',
      strtotime($row->getSourceProperty('date_heure'))
    );
    $row->setSourceProperty(
      'changed',
      strtotime($row->getSourceProperty('maj'))
    );

    return parent::prepareRow($row);
  }

}
