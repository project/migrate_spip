<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for Sections.
 *
 * @MigrateSource(
 *   id = "migrate_spip_sections"
 * )
 */
class Sections extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'rubriques', 'r')
      ->fields('r', [
        'id_parent',
        'id_rubrique',
        'maj',
        'statut',
        'texte',
        'titre',
      ])
      // Import section without parent first to prevent skipping.
      ->orderBy('r.id_parent');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'id_parent' => $this->t('Parent ID'),
      'id_rubrique' => $this->t('Section ID'),
      'maj' => $this->t('Updated'),
      'statut' => $this->t('Status'),
      'texte' => $this->t('Body text'),
      'titre'  => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_rubrique' => [
        'type' => 'integer',
        'alias' => 'r',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $row->setSourceProperty(
      'changed',
      strtotime($row->getSourceProperty('maj'))
    );

    $row->setSourceProperty(
      'alias',
      $this->getUrl('rubrique', $row->getSourceProperty('id_rubrique'))
    );

    return parent::prepareRow($row);
  }

}
