<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for Media Remote Videos.
 *
 * @MigrateSource(
 *   id = "migrate_spip_media_remote_videos"
 * )
 */
class MediaRemoteVideos extends MediaBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return parent::query()
      ->fields('d', ['extension'])
      ->condition('extension', ['dist_youtu', 'dist_daily'], 'IN')
      ->condition('fichier', '%youtube%', 'NOT LIKE')
      ->condition('fichier', '%dailymotion%', 'NOT LIKE');
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return parent::fields() + [
      'extension' => $this->t('Extension'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $file = $row->getSourceProperty('fichier');
    $extension = $row->getSourceProperty('extension');

    $temp = explode('/', $file);
    $file = end($temp);

    if (!empty($file)) {
      $url = match($extension) {
        'dist_youtu' => 'https://www.youtube.com/watch?v=' . $file,
        'dist_daily' => 'https://www.dailymotion.com/video/' . $file,
        default => NULL,
      };

      if (isset($url)) {
        $row->setSourceProperty('url', $url);
      }
    }

    return parent::prepareRow($row);
  }

}
