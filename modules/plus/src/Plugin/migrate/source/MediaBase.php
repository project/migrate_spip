<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_plus\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;

/**
 * Base source plugin for medias.
 */
abstract class MediaBase extends MigrateSpipBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'documents', 'd')
      ->fields('d', [
        'fichier',
        'id_document',
        'maj',
        'statut',
        'titre',
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'fichier' => $this->t('File'),
      'id_document' => $this->t('File ID'),
      'maj' => $this->t('Updated'),
      'statut' => $this->t('Status'),
      'titre' => $this->t('Title'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    return [
      'id_document' => [
        'type' => 'integer',
        'alias' => 'd',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {
    $title = $row->getSourceProperty('titre');

    if (empty($title)) {
      $title = $row->getSourceProperty('fichier');
    }

    $row->setSourceProperty('titre', mb_substr($title, 0, 255));

    return parent::prepareRow($row);
  }

}
