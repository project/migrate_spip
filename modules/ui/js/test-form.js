/**
 * @file test-form.js
 */

((Drupal) => {
  "use strict";

  Drupal.behaviors.migrateSpipUiTestForm = {
    attach: function (context) {
      const form = context.querySelector(".migrate-spip-ui-test-form");
      if (!form) {
        return;
      }

      const textarea = form.querySelector("textarea");
      if (!textarea) {
        return;
      }

      // Removal of the "cols" and "rows" attributes to allow
      // the textarea to take the full width and height of the cell.
      textarea.removeAttribute("cols");
      textarea.removeAttribute("rows");
    }
  };

})(Drupal);
