<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\migrate_spip\SpipRichTextManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the site configuration form.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The SPIP rich text manager.
   *
   * @var \Drupal\migrate_spip\SpipRichTextManagerInterface
   *
   * @SuppressWarnings(PHPMD.LongVariable)
   */
  protected SpipRichTextManagerInterface $spipRichTextManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->spipRichTextManager = $container->get('plugin.manager.spip_rich_text');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'migrate_spip_ui_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['migrate_spip.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getEditableConfigNames()[0]);

    $form['tables_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tables prefix'),
      '#description' => $this->t('Prefix of SPIP tables in the database (“spip” by default).'),
      '#default_value' => $config->get('tables_prefix'),
      '#required' => TRUE,
    ];

    $options = [];
    foreach ($this->spipRichTextManager->getDefinitions() as &$definition) {
      $options[$definition['id']] = $definition['label'];
    }
    asort($options);

    $form['plugins_disabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disabled plugins'),
      '#description' => $this->t('Check available plugins to disable. A disabled plugin will not be executed when converting text.'),
      '#options' => $options,
      '#default_value' => $config->get('plugins_disabled') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config($this->getEditableConfigNames()[0])
      ->set('tables_prefix', $form_state->getValue('tables_prefix'))
      ->set('plugins_disabled', Checkboxes::getCheckedCheckboxes($form_state->getValue('plugins_disabled')))
      ->save();
  }

}
