<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_spip\SpipRichTextManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Migrate SPIP test form.
 */
class TestForm extends FormBase {

  /**
   * The default text test (before submission).
   *
   * @var string
   */
  const FILEPATH = __DIR__ . '/../../../../tests/data/spip-rich-text.txt';

  /**
   * The SPIP rich text manager.
   *
   * @var \Drupal\migrate_spip\SpipRichTextManagerInterface
   *
   * @SuppressWarnings(PHPMD.LongVariable)
   */
  protected SpipRichTextManagerInterface $spipRichTextManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->spipRichTextManager = $container->get('plugin.manager.spip_rich_text');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'migrate_spip_ui_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $userInput = $form_state->getUserInput();
    $text = $userInput['table']['test']['text'] ?? NULL;

    if (!isset($text)) {
      $handle = fopen(static::FILEPATH, 'r');

      if ($handle) {
        $text = fread(
          $handle,
          filesize(static::FILEPATH)
        );
      }
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Before'),
        $this->t('After'),
      ],
    ];

    $form['table']['test']['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SPIP rich text to convert'),
      '#title_display' => 'invisible',
      '#default_value' => $text,
      '#required' => TRUE,
    ];

    $form['table']['test']['html'] = [
      '#type' => 'html_tag',
      '#tag' => 'code',
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
      '#value' => htmlentities($this->spipRichTextManager->applyAll($text)),
    ];

    $form['actions_header'] = [
      '#type' => 'actions',
      '#weight' => -100,
    ];

    $form['actions_footer'] = [
      '#type' => 'actions',
      '#weight' => 100,
    ];

    $form['actions_header']['submit'] =
    $form['actions_footer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
    ];

    $form['#attached']['library'][] = 'migrate_spip_ui/test-form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild();
  }

}
