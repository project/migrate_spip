<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP TarteAuCitron videos for Facebook into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_tac_videos_facebook_video",
 *   label = @Translation("Media TarteAuCitron videos for Facebook"),
 *   weight = -40
 * )
 */
final class MediaTacVideosFacebook extends MediaTacVideosBase {

  /**
   * {@inheritdoc}
   */
  const PROVIDER = 'facebook_video';

}
