<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Manage SPIP videos into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_videos",
 *   label = @Translation("Media videos"),
 *   weight = -40
 * )
 */
final class MediaVideos extends MediaBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function applyMediaAlignment(string $text, ?string $align = NULL): string {
    /*
     * Search "<video123>" SPIP tags.
     *
     * Consider that you have previously migrated SPIP videos
     * to media using a "media_videos" migration task.
     *
     * @see migrate_spip_plus
     */
    if (
      !preg_match_all('#<video(.*?)' . ($align ? '\|' . $align : '') . '>#', $text, $matches) ||
      !$this->connection->schema()->tableExists('migrate_map_media_videos')
    ) {
      return $text;
    }

    foreach ($matches[1] as $index => $match) {
      $replace = '';

      if (!empty($match)) {
        $destId1 = $this->connection->select('migrate_map_media_videos', 'v')
          ->condition('sourceid1', $match)
          ->fields('v', ['destid1'])
          ->execute()
          ->fetchField();

        if (!empty($destId1)) {
          /** @var \Drupal\media\MediaInterface|null $media */
          $media = $this->entityTypeManager->getStorage('media')
            ->load($destId1);

          if (isset($media)) {
            $replace = $this->getMediaTag($media, $align);
          }
        }
      }

      $text = str_replace(
        $matches[0][$index],
        $replace,
        $text
      );
    }

    return $text;
  }

}
