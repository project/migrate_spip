<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Manage SPIP documents into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_documents",
 *   label = @Translation("Media documents"),
 *   weight = -40
 * )
 */
final class MediaDocuments extends MediaBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function applyMediaAlignment(string $text, ?string $align = NULL): string {
    /*
     * Search "<doc123|center>" SPIP tags.
     *
     * Consider that you have previously migrated SPIP documents
     * to media using a "media_documents" migration task.
     *
     * @see migrate_spip_plus
     */
    if (
      !preg_match_all('#<doc([^|>]+)' . ($align ? '\|' . $align : '') . '>#s', $text, $matches) ||
      !$this->connection->schema()->tableExists('migrate_map_media_documents')
    ) {
      return $text;
    }

    foreach ($matches[1] as $index => $match) {
      $replace = '';

      if (!empty($match)) {
        $destId1 = $this->connection->select('migrate_map_media_documents', 'mmd')
          ->condition('mmd.sourceid1', $match)
          ->fields('mmd', ['destid1'])
          ->execute()
          ->fetchField();

        if (!empty($destId1)) {
          /** @var \Drupal\media\MediaInterface|null $media */
          $media = $this->entityTypeManager->getStorage('media')
            ->load($destId1);

          if ($media) {
            $replace = $this->getMediaTag($media, $align);
          }
        }
      }

      $text = str_replace(
        $matches[0][$index],
        $replace,
        $text
      );
    }

    return $text;
  }

}
