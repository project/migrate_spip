<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP internal links for sections.
 *
 * Need to be executed after links.
 *
 * @SpipRichText(
 *   id = "links_internal_sections",
 *   label = @Translation("Links internal sections"),
 *   weight = -5
 * )
 */
final class LinksInternalSections extends LinksInternalBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function apply(string $text): string {
    if (!preg_match_all('#href="(\s*rub\s*(\d+)\s*)"#s', $text, $matches)) {
      return $text;
    }

    foreach ($matches[2] as $index => $match) {
      $text = str_replace(
        $matches[1][$index],
        $this->getUrl('rubrique', $match),
        $text
      );
    }

    return $text;
  }

}
