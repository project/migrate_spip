<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP footnotes.
 *
 * @SpipRichText(
 *   id = "footnotes",
 *   label = @Translation("Footnotes")
 * )
 */
final class Footnotes extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      '#\[\[(.*?)\]\]#s',
      '<span class="tooltip-content">$1</span>',
      $text
    );
  }

}
