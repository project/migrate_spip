<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP internal links for articles.
 *
 * Need to be executed after links.
 *
 * @SpipRichText(
 *   id = "links_internal_articles",
 *   label = @Translation("Links internal articles"),
 *   weight = -5
 * )
 */
final class LinksInternalArticles extends LinksInternalBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    if (!preg_match_all('#href="(\s*art(?:icle)?\s*(\d+)\s*)"#s', $text, $matches)) {
      return $text;
    }

    foreach ($matches[2] as $index => $match) {
      $text = str_replace(
        $matches[1][$index],
        $this->getUrl('article', $match),
        $text
      );
    }

    return $text;
  }

}
