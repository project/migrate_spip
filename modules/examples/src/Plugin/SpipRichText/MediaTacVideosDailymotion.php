<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP TarteAuCitron videos for Dailymotion into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_tac_videos_dailymotion",
 *   label = @Translation("Media TarteAuCitron videos for Dailymotion"),
 *   weight = -40
 * )
 */
final class MediaTacVideosDailymotion extends MediaTacVideosBase {

  /**
   * {@inheritdoc}
   */
  const PROVIDER = 'dailymotion';

}
