<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP TarteAuCitron videos for Vimeo into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_tac_videos_vimeo",
 *   label = @Translation("Media TarteAuCitron videos for Vimeo"),
 *   weight = -40
 * )
 */
final class MediaTacVideosVimeo extends MediaTacVideosBase {

  /**
   * {@inheritdoc}
   */
  const PROVIDER = 'vimeo';

}
