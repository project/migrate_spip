<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

/**
 * Manage SPIP TarteAuCitron videos for Youtube into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_tac_videos_youtube",
 *   label = @Translation("Media TarteAuCitron videos for Youtube"),
 *   weight = -40
 * )
 */
final class MediaTacVideosYoutube extends MediaTacVideosBase {

  /**
   * {@inheritdoc}
   */
  const PROVIDER = 'youtube';

}
