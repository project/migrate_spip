<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\Core\Database\Database;
use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP internal links base class.
 */
abstract class LinksInternalBase extends SpipRichTextBase {

  /**
   * Get object URL.
   *
   * @param string $objectType
   *   The object's type.
   * @param string $objectId
   *   The object's identifier.
   *
   * @return string
   *   The URL.
   */
  protected function getUrl(string $objectType, string $objectId): string {
    /*
     * Consider that the connection identifiers are available
     * in the "migrate" database key.
     */
    $urlInfo = Database::getConnection('default', 'migrate')->select($this->migrateSpipHelper->getDatabaseTablesPrefix() . 'urls', 'su')
      ->fields('su', ['id_parent', 'url'])
      ->condition('su.id_objet', $objectId)
      ->condition('su.type', $objectType)
      ->orderBy('su.date', 'DESC')
      ->execute()
      ->fetchObject();

    $url = '';
    if ($urlInfo) {
      // SPIP url doesn't contain slash prefix. Add it and escape it in case.
      $url = '/' . ltrim($urlInfo->url, '/');

      if ($urlInfo->id_parent !== '0') {
        // Parent are sections only.
        $url = $this->getUrl('rubrique', $urlInfo->id_parent) . $url;
      }
    }

    return $url;
  }

}
