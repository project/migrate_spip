<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Manage SPIP image into media.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "media_images",
 *   label = @Translation("Media images"),
 *   weight = -40
 * )
 */
final class MediaImages extends MediaBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function applyMediaAlignment(string $text, ?string $align = NULL): string {
    /*
     * Search "<img123|center>" SPIP tags.
     *
     * Consider that you have previously migrated SPIP images
     * to media using a "media_images" migration task.
     *
     * @see migrate_spip_plus
     */
    if (
      !preg_match_all('#<img([^|>]+)' . ($align ? '\|' . $align : '') . '>#s', $text, $matches) ||
      !$this->connection->schema()->tableExists('migrate_map_media_images')
    ) {
      return $text;
    }

    foreach ($matches[1] as $index => $match) {
      $replace = '';

      if (!empty($match)) {
        $destId1 = $this->connection->select('migrate_map_media_images', 'mmmi')
          ->condition('mmmi.sourceid1', $match)
          ->fields('mmmi', ['destid1'])
          ->execute()
          ->fetchField();

        if (!empty($destId1)) {
          /** @var \Drupal\media\MediaInterface|null $media */
          $media = $this->entityTypeManager->getStorage('media')
            ->load($destId1);

          if (isset($media)) {
            $replace = $this->getMediaTag($media, $align);
          }
        }
      }

      $text = str_replace(
        $matches[0][$index],
        $replace,
        $text
      );
    }

    return $text;
  }

}
