<?php

declare(strict_types=1);

namespace Drupal\migrate_spip_examples\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP boxed text.
 *
 * Need to be executed before tables.
 *
 * @SpipRichText(
 *   id = "boxed_text",
 *   label = @Translation("Boxed text"),
 *   weight = -40
 * )
 */
final class BoxedText extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        '#<cadre>(.*?)<cadre>#s',
        '#\|\[\((.*?)\)\]\|#s',
        '#\[\((.*?)\)\]#s',
      ],
      '<div class="cadre">$1</div>',
      $text
    );
  }

}
