# Migrate SPIP


## Description

Provides a SPIP rich text converter into HTML.

Using the [jQuery MarkItUp](https://markitup.jaysalvat.com/home/) editor,
the SPIP's rich text editor is not based on known standards (HTML, BBcode, Markdown, etc.).
It is therefore quite difficult to convert the rich text of SPIP into HTML.

Based on SPIP's [Textwheel engine](https://contrib.spip.net/Presentation-de-TextWheel),
this module incorporates the basic functionalities of SPIP's rich text editor conversion
in order to allow you to obtain clean HTML that you will insert into Drupal formatted fields.

## Submodules

Sub-modules are offered to support you in your migration:

* **Migrate SPIP UI:** Provides an interface to manage Migrate SPIP settings and easy test conversion.
* **Migrate SPIP Examples:** Provides plugin examples to extend "Migrate SPIP" module.
* **Migrate SPIP Plus:** Based on [Migrate Plus](https://www.drupal.org/project/migrate_plus)
module, provides optional basic configurations for SPIP
models ("articles", "rubriques", "noisettes").


## Requirements

* PHP >=8.0

Developed and tested on several SPIP 4.x projects, this module should be
compatible for older versions (at least SPIP 3.0).


## Installation

* Install and enable this module like any other Drupal 8, 9 or 10 module.


## Configuration

### Plugins disabling

By default, all conversion plugins are active.
You can deactivate it at any time from the module configuration.

### SPIP tables prefix

By default, SPIP tables are prefixed with the value "spip".
If the SPIP site that you wish to migrate uses another prefix,
you can change this prefix in the module configuration.

## Rich text

### User interface

By activating the "Migrate SPIP UI" module, you can configure
which plugins to use through an interface.
Otherwise, you will have to do it manually in the "migrate_spip.settings"
configuration file.

### Easy testing

A test page is available with "Migrate SPIP UI" module to easy check
rich text conversion.

Enable User Interface module and go to
"/admin/config/system/migrate-spip/test" page.


## Examples

This module provides import proposals from SPIP
(see the submodules "migrate_spip_plus" and "migrate_spip_examples).

It is a good basis for migrating "basic" data from SPIP.
Depending on your Drupal functionalities and your needs,
you will have to make them evolve.

These submodules notably offer bases for importing:
* SPIP Articles: into an "article" node type
* SPIP Forum: into default comment type
* SPIP Keywords: into a "tag" vocabulary and mapped with articles
* SPIP Sections: into a "section" vocabulary and mapped with articles
* SPIP Documents:
  * Files into a "document" media type
  * Images: into an "image" media type
  * Remote videos: into an "video" media type
* SPIP Users

### Limitations

* Some features are not possible in Drupal:
For example in SPIP an article can have multiple authors.
In drupal this functionality is not available out-of-the-box.

* Some features are not in core minimal but can be found in core or contributed modules:
for example the status of articles: in SPIP they are (in progress, for evaluation, published, ..).
Available status in drupal core are published/unpublished.
This module just do a reduction of spip statuses to drupal ones.

* User passwords are hashed in SHA256 with a salt key. The salt key is unique and password can't
be imported as the encryption mechanism of SPIP without give "secret_des_auth" key of you site instance.
See: [\Spip\Chiffrer\Password::hacher()](https://github.com/spip/SPIP/blob/master/ecrire/src/Chiffrer/Password.php#L41)
This key is available in the file `config/cles.php` of your SPIP source code.

## Official documentation

* **SPIP (in French):** https://contrib.spip.net/Documentation
* **SPIP TextWheel (in French):** https://contrib.spip.net/Presentation-de-TextWheel
* **jQuery MarkItUp editor:** https://markitup.jaysalvat.com/home/
* **SPIP code:** https://code.spip.net/

## Maintainers

* Sébastien Brindle (S3b0uN3t) - https://www.drupal.org/user/2989133
