<?php

declare(strict_types=1);

namespace Drupal\migrate_spip;

/**
 * Interface for "SpipRichText" annotated plugin.
 */
interface SpipRichTextInterface {

  /**
   * Constants extracted from SPIP and needed by several plugins.
   *
   * @var string
   */
  const AUTOBR = '<br class="autobr" />';
  const HTML_TAGS_BLOCK = 'address|applet|article|aside|bloc|blockquote|button|center|d[ltd]|div|fieldset|fig(ure|caption)|footer|form|h[1-6r]|hgroup|head|header|iframe|li|map|marquee|nav|noscript|object|ol|pre|section|t(able|body|head|d|r|extarea)|ul|section|script|style|drupal-media';

  /**
   * Apply plugin instance converting for SPIP rich text format.
   *
   * @param string $text
   *   The plain text.
   *
   * @return string
   *   The processed text.
   */
  public function apply(string $text): string;

}
