<?php

declare(strict_types=1);

namespace Drupal\migrate_spip;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for SPIP rich text manager.
 */
interface SpipRichTextManagerInterface extends PluginManagerInterface {

  /**
   * Apply plugin instance converting for SPIP rich text format.
   *
   * @param string $plugin_id
   *   The ID of the plugin being instantiated.
   * @param string $text
   *   The plain text.
   *
   * @return string
   *   The processed text.
   */
  public function apply(string $plugin_id, string $text): string;

  /**
   * Apply all plugin instances converting for SPIP rich text format.
   *
   * @param string $text
   *   The plain text.
   *
   * @return string
   *   The processed text.
   */
  public function applyAll(string $text): string;

  /**
   * Apply some plugin instances converting for SPIP rich text format.
   *
   * Preserves plugin ordering.
   *
   * @param string[] $plugin_ids
   *   The ID of the plugins being instantiated.
   * @param string $text
   *   The plain text.
   *
   * @return string
   *   The processed text.
   */
  public function applySome(array $plugin_ids, string $text): string;

}
