<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP subheadings.
 *
 * @see https://contrib.spip.net/Intertitres
 *
 * @SpipRichText(
 *   id = "subheadings",
 *   label = @Translation("Subheadings"),
 *   weight = -20
 * )
 */
final class SubHeadings extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    // Manage complex subheading.
    return preg_replace(
      [
        // Heading 6.
        '#(^|[^{])\|?\{\{\{\*{5}\s*(.*?)\s*}}}($|[^}])#s',
        // Heading 5.
        '#(^|[^{])\|?\{\{\{\*{4}\s*(.*?)\s*}}}($|[^}])#s',
        // Heading 4.
        '#(^|[^{])\|?\{\{\{\*{3}\s*(.*?)\s*}}}($|[^}])#s',
        // Heading 3.
        '#(^|[^{])\|?\{\{\{\*{2}\s*(.*?)\s*}}}($|[^}])#s',
        // Heading 2.
        '#(^|[^{])\|?\{\{\{\*?\s*(.*?)\s*}}}($|[^}])#s',
      ],
      [
        '$1<h6>$2</h6>$3',
        '$1<h5>$2</h5>$3',
        '$1<h4>$2</h4>$3',
        '$1<h3>$2</h3>$3',
        '$1<h2>$2</h2>$3',
      ],
      $text
    );
  }

}
