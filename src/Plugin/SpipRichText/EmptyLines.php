<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Replace empty lines.
 *
 * @SpipRichText(
 *   id = "empty_lines",
 *   label = @Translation("Empty lines"),
 *   weight = -40
 * )
 */
final class EmptyLines extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      '#^[ \t]+$#m',
      '',
      $text
    );
  }

}
