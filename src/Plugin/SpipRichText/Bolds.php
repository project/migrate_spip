<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP bolds.
 *
 * Need to be executed after subheadings.
 *
 * @SpipRichText(
 *   id = "bolds",
 *   label = @Translation("Bolds"),
 *   weight = -19
 * )
 */
final class Bolds extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return str_replace(
      [
        // Empty bold.
        '{{}}',
        '{{',
        '}}',
      ],
      [
        '',
        '<strong>',
        '</strong>',
      ],
      $text
    );
  }

}
