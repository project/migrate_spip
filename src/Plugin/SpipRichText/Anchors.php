<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP anchors.
 *
 * Need to be executed before Links.
 *
 * @SpipRichText(
 *   id = "anchors",
 *   label = @Translation("Anchors"),
 *   weight = -22
 * )
 */
final class Anchors extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      // SPIP generates the anchors in the form of a link.
      // However, this is not a good practice in terms of accessibility.
      // Use "span" HTML tag instead.
      '#\[\#((?!<-).*?)<-]#mS',
      '<span id="$1"></span>',
      $text
    );
  }

}
