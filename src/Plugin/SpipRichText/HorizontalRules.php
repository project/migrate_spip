<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP horizontal rules.
 *
 * Need to be executed before EmDashes.
 *
 * @SpipRichText(
 *   id = "horizontal_rules",
 *   label = @Translation("Horizontal rules"),
 *   weight = -21
 * )
 */
final class HorizontalRules extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      "#(^|\n)(----+|____+)#S",
      "\n\n\n<hr />\n\n\n",
      $text
    );
  }

}
