<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP em dashes.
 *
 * @SpipRichText(
 *   id = "em_dashes",
 *   label = @Translation("Em dashes"),
 *   weight = -20
 * )
 */
final class EmDashes extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      '#\n-- *#',
      "\n<br />&mdash;&nbsp;",
      $text
    );
  }

}
