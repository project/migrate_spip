<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage line breaks.
 *
 * Need to be executed before paragraphs.
 *
 * @SpipRichText(
 *   id = "line_breaks",
 *   label = @Translation("Line breaks"),
 *   weight = -40
 * )
 */
final class LineBreaks extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        ",\r\n?,S",
        // Line break useless.
        "#(^|^\n|\n\n)_ +#S",
        // Correct line break.
        "#\n_ +#S",
      ],
      [
        "\n",
        "$1",
        '<br class="manualbr" />',
      ],
      $text
    );
  }

}
