<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Replace quotes.
 *
 * @SpipRichText(
 *   id = "quotes",
 *   label = @Translation("Quotes"),
 *   weight = -19
 * )
 */
final class Quotes extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return str_replace(
      [
        '<quote>',
        '</quote>',
      ],
      [
        '<blockquote>',
        '</blockquote>',
      ],
      $text
    );
  }

}
