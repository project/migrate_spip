<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP preformated code.
 *
 * @SpipRichText(
 *   id = "preformated_code",
 *   label = @Translation("Preformated code"),
 *   weight = -10
 * )
 */
final class PreFormatedCode extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    // Manage block code first.
    $text = preg_replace_callback(",`{3}\s*(.*?)\s*`{3},S", [self::class, 'applyCodeBlock'], $text);

    // Manage block code first.
    return preg_replace_callback(",`(.*?)`,sS", [self::class, 'applyCodeInline'], $text);
  }

  /**
   * Format code block.
   *
   * @param array $match
   *   The matched array.
   *
   * @return string
   *   The replaced string.
   *
   * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
   */
  private static function applyCodeBlock(array $match): string {
    return '<pre>' . static::applyCodeInline($match) . '</pre>';
  }

  /**
   * Format code inline.
   *
   * @param array $match
   *   The matched array.
   *
   * @return string
   *   The replaced string.
   */
  private static function applyCodeInline(array $match): string {
    return '<code>' . str_replace("\n", '', htmlentities($match[1])) . '</code>';
  }

}
