<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP abbreviations.
 *
 * @SpipRichText(
 *   id = "abbreviations",
 *   label = @Translation("Abbreviation"),
 *   weight = -20
 * )
 */
final class Abbreviations extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      '#\[abbr\|(.*)\]#is',
      '<abbr>$1</abbr>',
      $text
    );
  }

}
