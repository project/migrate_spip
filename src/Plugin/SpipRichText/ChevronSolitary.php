<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Replace chevron solitary to prevent opened tag.
 *
 * @SpipRichText(
 *   id = "chevron_solitary",
 *   label = @Translation("Chevron solitary")
 * )
 */
final class ChevronSolitary extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        '#<(?=[^a-z/!])#Uims',
        '#<([^><"\'!]*)(?=<|$)#Uims',
      ],
      [
        '&lt;',
        '&lt;$1',
      ],
        $text
    );
  }

}
