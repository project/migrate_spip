<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP text centering.
 *
 * @SpipRichText(
 *   id = "align_center",
 *   label = @Translation("Align center"),
 *   weight = -20
 * )
 */
final class AlignCenter extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        '#\[\|(.*)\|\]#s',
        '#<center>(.*?)</center>#s',
      ],
      '<p class="text-align-center">$1</p>',
      $text
    );
  }

}
