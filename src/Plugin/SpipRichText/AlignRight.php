<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP text right alignement.
 *
 * @SpipRichText(
 *   id = "align_right",
 *   label = @Translation("Align right"),
 *   weight = -20
 * )
 */
final class AlignRight extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      '#\[/(.*)/]#s',
      '<p class="text-align-right">$1</p>',
      $text
    );
  }

}
