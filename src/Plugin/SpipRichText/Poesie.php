<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP "<poesie></poesie>" tags.
 *
 * @SpipRichText(
 *   id = "poesie",
 *   label = @Translation("Poesie"),
 *   weight = -40
 * )
 */
final class Poesie extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    preg_match_all(",<poe(sie|try)>(.*)</poe(sie|try)>,UimsS", $text, $matches);

    foreach ($matches[2] as $index => $text2) {
      $text2 = preg_replace(
        [
          "#\r\n?#S",
          "#\n[\s]*\n#Su",
          "#\n+#",
        ],
        [
          "\n",
          "\n&nbsp;\n",
          "</p>\n<p>",
        ],
        $text2
      );

      $text = str_replace(
        $matches[0][$index],
        '<blockquote class="poesie"><p>' . trim($text2) . "</p></blockquote>\n\n",
        $text
      );
    }

    return $text;
  }

}
