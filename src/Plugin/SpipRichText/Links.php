<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP links.
 *
 * Need to be executed before lists.
 *
 * @SpipRichText(
 *   id = "links",
 *   label = @Translation("Links"),
 *   weight = -21
 * )
 */
final class Links extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        // Link with title attribute and language (eq. [label|title{lang}->url]).
        '#\[([^|{-].*?)\|([^{-].*?)\{([^}].*?)}->([^]]+)]#mS',
        // Link with title attribute (eq. [label|title->url]).
        '#\[([^|-]+)\|((?!->).*?)->([^]]+)]#mS',
        // Link with language (eq. [label{lang}->url]).
        '#\[([^|{-].*?)\{([^}].*?)}->([^]]+)]#mS',
        // Link (simple) (eq. [label->url]).
        '#\[((?!->).*?)->([^]].*?)]#mS',
        // Link without label (eq. [->url]).
        '#\[->([^]].*?)]#mS',
        // Link anchor (eq. [id<-])
        '#\[((?!<-).*?)<-]#mS',
      ],
      [
        '<a href="$4" title="$2" lang="$3">$1</a>',
        '<a href="$3" title="$2">$1</a>',
        '<a href="$3" lang="$2">$1</a>',
        '<a href="$2">$1</a>',
        '<a href="$1">$1</a>',
        '<a id="$1"></a>',
      ],
      $text
    );
  }

}
