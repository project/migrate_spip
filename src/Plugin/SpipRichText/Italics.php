<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Manage SPIP italics.
 *
 * Need to be executed after subheadings, bolds and links.
 *
 * @SpipRichText(
 *   id = "italics",
 *   label = @Translation("Italics"),
 *   weight = -18
 * )
 */
final class Italics extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return str_replace(
      [
        // Empty italic.
        '{}',
        '{',
        '}',
      ],
      [
        '',
        '<em>',
        '</em>',
      ],
      $text
    );
  }

}
