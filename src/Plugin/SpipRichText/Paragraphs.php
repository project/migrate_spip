<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\SpipRichText;

use Drupal\migrate_spip\SpipRichTextBase;

/**
 * Open paragraphs.
 *
 * @SpipRichText(
 *   id = "paragraphs",
 *   label = @Translation("Paragraphs"),
 *   weight = -10
 * )
 */
final class Paragraphs extends SpipRichTextBase {

  /**
   * {@inheritdoc}
   */
  public function apply(string $text): string {
    return preg_replace(
      [
        "# *\n(?: *\n)+(?:<br ?\/?>)*#S",
        "#(?:<br\b[^>]*>){2,}\s*#S",
        "#(<p\b[^>]*>)\n*(?:<br ?/?>\n*)+#S",
      ],
      [
        '<p>',
        '<p>',
        '$1',
      ],
      $text
    );
  }

}
