<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_spip\SpipRichTextManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process SPIP rich text field value (like "texte") to HTML.
 *
 * @MigrateProcessPlugin(
 *   id = "spip_rich_text"
 * )
 */
final class SpipRichText extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The SPIP rich text manager.
   *
   * @var \Drupal\migrate_spip\SpipRichTextManagerInterface
   *
   * @SuppressWarnings(PHPMD.LongVariable)
   */
  protected SpipRichTextManagerInterface $spipRichTextManager;

  /**
   * Constructs an SpipRichText plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_spip\SpipRichTextManagerInterface $spip_rich_text_manager
   *   The SPIP rich text manager.
   *
   * @SuppressWarnings(PHPMD.LongVariable)
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    SpipRichTextManagerInterface $spip_rich_text_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->spipRichTextManager = $spip_rich_text_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.spip_rich_text')
    );
  }

  /**
   * SPIP rich text field value (like "texte") to HTML.
   *
   * {@inheritdoc}
   *
   * @throws \Drupal\migrate\MigrateException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_string($value)) {
      throw new MigrateException('The input value must be a string.');
    }

    // In some cases, we want to apply some plugins (to increase performance).
    // Allow to apply SPIP rich text converter for a defined list of plugins.
    if (
      !empty($this->configuration['plugins']) &&
      is_array($this->configuration['plugins'])
    ) {
      return $this->spipRichTextManager->applySome($this->configuration['plugins'], $value);
    }

    return $this->spipRichTextManager->applyAll($value);
  }

}
