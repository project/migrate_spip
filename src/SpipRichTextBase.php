<?php

declare(strict_types=1);

namespace Drupal\migrate_spip;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\migrate_spip\Helper\MigrateSpipHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for "SpipRichText" annotated plugin.
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class SpipRichTextBase extends PluginBase implements SpipRichTextInterface, ContainerFactoryPluginInterface {

  /**
   * The SPIP migration helper.
   *
   * @var \Drupal\migrate_spip\Helper\MigrateSpipHelperInterface
   */
  protected MigrateSpipHelperInterface $migrateSpipHelper;

  /**
   * Creates a SpipRichText instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate_spip\Helper\MigrateSpipHelperInterface $migrate_spip_helper
   *   The SPIP migration helper.
   */
  final public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    MigrateSpipHelperInterface $migrate_spip_helper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migrateSpipHelper = $migrate_spip_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate_spip.helper')
    );
  }

}
