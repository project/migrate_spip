<?php

declare(strict_types=1);

namespace Drupal\migrate_spip;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\migrate\MigrateException;

/**
 * Provides a "SpipRichTextManager" plugin manager.
 *
 * @see \Drupal\migrate_spip\Annotation\SpipRichText
 * @see \Drupal\migrate_spip\SpipRichTextInterface
 * @see plugin_api
 */
class SpipRichTextManager extends DefaultPluginManager implements SpipRichTextManagerInterface {

  /**
   * The "mirate_spip" module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a "SpipRichTextManager" object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct(
      'Plugin/SpipRichText',
      $namespaces,
      $module_handler,
      'Drupal\migrate_spip\SpipRichTextInterface',
      'Drupal\migrate_spip\Annotation\SpipRichText'
    );

    $this->config = $config_factory->get('migrate_spip.settings');

    $this->alterInfo('spip_rich_text_info');
    $this->setCacheBackend($cache_backend, 'spip_rich_text_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions(): array {
    $definitions = parent::findDefinitions();
    $pluginsDisabled = $this->config->get('plugins_disabled');

    foreach ($definitions as $index => $definition) {
      // If plugin is disabled, remove it definition.
      if (in_array($definition['id'], $pluginsDisabled)) {
        unset($definitions[$index]);
      }
    }

    // Sort plugins by weight.
    uasort($definitions, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    return $definitions;
  }

  /**
   * Get all plugin instances.
   *
   * @return array
   *   The plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getInstances(): array {
    static $instances;

    if ($instances !== NULL) {
      return $instances;
    }

    $instances = [];
    foreach (array_keys($this->getDefinitions()) as &$plugin_id) {
      $instances[$plugin_id] = $this->createInstance($plugin_id);
    }

    return $instances;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function apply(string $plugin_id, string $text): string {
    return $this->createInstance($plugin_id)->apply($text);
  }

  /**
   * {@inheritdoc}
   */
  public function applyAll(string $text): string {
    foreach ($this->getInstances() as $instance) {
      $text = $instance->apply($text);
    }

    return $text;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  public function applySome(array $plugin_ids, string $text): string {
    $instances = $this->getInstances();

    foreach ($plugin_ids as $plugin_id) {
      if (!isset($instances[$plugin_id])) {
        throw new MigrateException(
          sprintf('The convert rich text plugin "%s" does not exist.', $plugin_id)
        );
      }

      $text = $instances[$plugin_id]->apply($text);
    }

    return $text;
  }

}
