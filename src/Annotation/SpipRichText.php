<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a SpipRichText annotation object.
 *
 * @ingroup spip_rich_text
 *
 * @Annotation
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class SpipRichText extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the SPIP rich text plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The weight of the plugin, as it should be ordered.
   *
   * @var int
   */
  public int $weight = 0;

}
