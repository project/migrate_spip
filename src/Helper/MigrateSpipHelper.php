<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * SPIP migration helper.
 */
class MigrateSpipHelper implements MigrateSpipHelperInterface {

  /**
   * The "mirate_spip" module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a "MigrateSpipHelper" object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('migrate_spip.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseTablesPrefix(): string {
    return $this->config->get('tables_prefix') . '_';
  }

}
