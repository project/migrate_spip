<?php

declare(strict_types=1);

namespace Drupal\migrate_spip\Helper;

/**
 * Interface for SPIP migration helper.
 */
interface MigrateSpipHelperInterface {

  /**
   * Get the SPIP database tables prefix.
   *
   * @return string
   *   The tables prefix.
   */
  public function getDatabaseTablesPrefix(): string;

}
