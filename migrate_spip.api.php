<?php

/**
 * @file
 * Hooks related to "migrate_spip" API.
 */

declare(strict_types=1);

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided in \Drupal\migrate_spip\Annotation\SpipRichText.
 *
 * @param array $definitions
 *   The array of definition plugins, keyed by the machine-readable name.
 */
function hook_spip_rich_text_info_alter(array &$definitions) {
  $definitions['links']['class'] = 'Drupal\my_module\src\Plugin\SpipRichText\LinksExtends';
}

/**
 * @} End of "addtogroup hooks".
 */
